<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Swagger Basic Info
|--------------------------------------------------------------------------
| Example: https://github.com/zircote/swagger-php/blob/master/Examples/petstore.swagger.io/swagger-v2.php
*/

/**
 * @SWG\Swagger(
 *	@SWG\Info(
 *		version="0.1",
 *		title="Framework_master API",
 *		description="API Documentation",
 *		termsOfService="",
 *		@SWG\Contact(
 *			name="Framework_master",
 *			email="jodyaryono@gmail.com"
 *		)
 *	),
 * 	schemes={API_PROTOCOL},
 *  host=API_HOST,
 *  basePath="/api"
 * )
 */
