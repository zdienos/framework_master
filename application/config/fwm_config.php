<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| CI Bootstrap 3 Configuration
| -------------------------------------------------------------------------
| This file lets you define default values to be passed into views
| when calling MY_Controller's render() function.
|
| See example and detailed explanation from:
*/

$config['fwm_config'] = array(

	// Site name
	'site_name' => 'CI Bootstrap 4 Admin LTE 3',

	// Default page title prefix
	'page_title_prefix' => 'Framework_master Admin LTE3',

	// Default page title
	'page_title' => 'Framework_master',

	// Default meta data
	'meta_data'	=> array(
		'author'		=> '',
		'description'	=> '',
		'keywords'		=> ''
	),

	// Default scripts to embed at page head or end
	'scripts' => array(
		'head'	=> array(
			'assets/dist/plugins/jquery/jquery.min.js',
		),
		'foot'	=> array(

			'assets/dist/plugins/bootstrap/js/bootstrap.bundle.min.js',
			'assets/dist/frontend/adminlte3/js/adminlte.min.js',
			'assets/dist/plugins/datatables/jquery.dataTables.min.js',

		),
	),

	// Default stylesheets to embed at page head
	'stylesheets' => array(
		'screen' => array(
			'assets/dist/plugins/fontawesome-free/css/all.min.css',
			'assets/dist/frontend/adminlte3/css/adminlte.min.css',
			'assets/dist/frontend/style.css',
			'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700'.
			'assets/dist/frontend/plugins/datatables/',
			'assets/dist/plugins/datatables-bs4/css/dataTables.bootstrap4.css'
		)
	),

	// Default CSS class for <body> tag
	'body_class' => 'hold-transition sidebar-mini',

	// Multilingual settings
	'languages' => array(
		'default'		=> 'id',
		'autoload'		=> array('general'),
		'available'		=> array(
			'en' => array(
				'label'	=> 'English',
				'value'	=> 'english'
			),
			'zh' => array(
				'label'	=> '繁體中文',
				'value'	=> 'traditional-chinese'
			),
			'cn' => array(
				'label'	=> '简体中文',
				'value'	=> 'simplified-chinese'
			),
			'es' => array(
				'label'	=> 'Español',
				'value' => 'spanish'
			),
			'id' => array(
				'label'	=> 'Indonesia',
				'value' => 'indonesian'
			)
		)
	),

	// Google Analytics User ID
	'ga_id' => '',

	// Menu items
	'menu' => array(
		'home' => array(
			'name'		=> 'home',
			'url'		=> '',
		),
	),

	// Login page
	'login_url' => '',

	// Restricted pages
	'page_auth' => array(
	),

	// Email config
	'email' => array(
		'from_email'		=> '',
		'from_name'			=> '',
		'subject_prefix'	=> '',

		// Mailgun HTTP API
		'mailgun_api'		=> array(
			'domain'			=> '',
			'private_api_key'	=> ''
		),
	),

	// Debug tools
	'debug' => array(
		'view_data'	=> FALSE,
		'profiler'	=> FALSE
	),
);

/*
| -------------------------------------------------------------------------
| Override values from /application/config/config.php
| -------------------------------------------------------------------------
*/
$config['sess_cookie_name'] = 'ci_session_frontend';
