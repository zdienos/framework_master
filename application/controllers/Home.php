<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {


	public function index($id=0,$state="new")
	{
		$this->load->model('Admin_user_model');
		$data=$this->Admin_user_model->dataMahasiswa();
		$this->mViewData['data']=$data;
		$nim="";
		$nama="";
		$telp="";

		if($this->input->post()){
			if($state=="edit"){


			}elseif($state=="delete"){

			}else{
				$nim="";
				$nama="";
				$telp="";
			}
		}else{
			if($id>0){
				//if edit
				$state="edit";
			}
		}
		$this->mViewData['state']=$state;
		$this->mViewData['nim']=$nim;
		$this->mViewData['nama']=$nama;
		$this->mViewData['telp']=$telp;
		$this->mViewData['data']=$data;
		$this->render('home');
	}

	public function tambah_siswa()
	{
		$this->mPageTitle="";
		$this->render('tambah_siswa');
	}
}
