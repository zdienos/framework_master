<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
  <form id="myform" name="myform" method="post">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Starter Page</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Starter Page</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h5 class="m-0">Siswa Entry/Edit</h5>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="nim">Nim</label>
                <input type="text" class="form-control" id="nim" name="nim" placeholder="Masukan NIM" value="<?php echo $nim ?>">
              </div>
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama" value="<?php echo $nama ?>">
              </div>
              <div class="form-group">
                <label for="telp">Telp</label>
                <input type="text" class="form-control" id="telp" name="telp" placeholder="Masukan Telp" value="<?php echo $telp ?>">
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
        <div class="col-lg-8">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h5 class="m-0">Siswa List</h5>
            </div>
            <div class="card-body">
              <table id="table_id" class="table table-bordered table-hover dataTable">
                <thead>
                  <tr>
                    <th>Nim</th>
                    <th>Nama</th>
                    <th>Telp</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data as $item): ?>
                    <tr>
                      <td><?php echo $item->nim ?></td>
                      <td><?php echo $item->nama ?></td>
                      <td><?php echo $item->telp ?></td>
                      <td>
                        <a href="http://localhost/starter/auth/edit_user/1" class="badge bg-blue">Edit</a>
                        <a href="#" data-id="1" data-name="Admin" class="badge bg-red delete-info" data-toggle="modal" data-target="#modal-default">Hapus</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>

              </table>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
  </form>
</div>
<!-- /.content-wrapper -->


</div>
<script type="text/javascript">
$(document).ready( function () {
  $('#table_id').DataTable();
} );
</script>
