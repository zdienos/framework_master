CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) DEFAULT NULL,
  `file_url` varchar(255) DEFAULT NULL,
  `width` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
INSERT INTO `db_fwmaster`.`media`(`id`, `identifier`, `file_url`, `width`, `height`) VALUES (1, 'logo', 'c3d3d-framework_master_orange.png', 220, NULL);
